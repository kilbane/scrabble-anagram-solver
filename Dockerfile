FROM ubuntu:22.04

RUN useradd -ms /bin/bash user
WORKDIR /home/user
COPY anagram .
COPY sowpods.txt .
RUN chmod +x anagram
USER user
